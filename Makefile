#
#
#
ROOTSYS		= /opt/root-$(VERSION).$(SUBVERSION)
VERSION		= 5.26
SUBVERSION	= 00a

libroot.spec:libroot.spec.in Makefile
	csplit -f libroot.spec. $< '/@\(PROVIDES\|LINKS\)@/' '{*}' 
	sed -e 's/@VERSION@/$(VERSION)/' \
		-e 's/@SUBVERSION@/$(SUBVERSION)/' \
		< libroot.spec.00 > $@ 
	for i in $(ROOTSYS)/lib/lib*.so.$(VERSION);do \
	  case $$i in *Dict.*) continue ;; esac ; \
	  b=`basename $$i` ; \
	  printf "Provides:\t%s\n" $$b >> $@ ; \
	  printf "ln -s $$i .\n" >> libroot.spec.01 ;  \
	done 
	sed '/@PROVIDES@/d' < libroot.spec.01 >> $@ 
	sed '/@LINKS@/d'    < libroot.spec.02 >> $@ 
	rm -f libroot.spec.00 libroot.spec.01 libroot.spec.02 

dist:	clean libroot.spec
	mkdir libroot-$(VERSION)
	cp Makefile libroot.spec libroot.spec.in libroot-$(VERSION)/
	tar -czvf libroot-$(VERSION).tar.gz libroot-$(VERSION)

rpm:	dist
	sudo rpmbuild -ta libroot-$(VERSION).tar.gz

rpm-nosu:dist
	rpmbuild -D "_topdir $(HOME)/rpm" -ta $(distdir).tar.gz

clean:
	rm -f libroot.spec.00 libroot.spec.01 libroot.spec.02 
	rm -f *~ libroot.spec

#
# EOF
#

